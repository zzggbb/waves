#!/bin/env python3

import sys
import time
import math
import struct

import numpy
import pygame
import pyaudio

import util

from slider import Slider
from controls import Controls
from stream import Stream
from output import Output

class Waves(object):
  def __init__(self):
    pygame.init()

    self.output = Output()
    self.stream = Stream(channels=1, sample_rate=60*10**3, sample_size=2**11)

    self.mouse_frequency = 0.0

    # visual params
    self.background_color = pygame.Color(50, 50, 50)
    self.colorA = pygame.Color("#ff0000")
    self.colorB = pygame.Color("#0000ff")

    # surface params
    self.height = 1000
    self.dimensions = numpy.array([self.output.width(), self.height])
    self.surface_flags = pygame.HWSURFACE | pygame.DOUBLEBUF
    self.surface = pygame.display.set_mode(self.dimensions, self.surface_flags)
    self.time_surface = pygame.Surface(self.dimensions // numpy.array([1,2]))
    self.freq_surface = pygame.Surface(self.dimensions // numpy.array([1,2]))
    self.control_surface = pygame.Surface(self.dimensions // 2)
    self.control_surface.set_colorkey(self.background_color)

    self.controls = Controls(self.control_surface)

    self.sliders = {
      'pull': Slider(self.control_surface,
        pygame.Rect(300, 46, 100, 10), 10, 15, value=0.8),

      'smooth': Slider(self.control_surface,
        pygame.Rect(300, 66, 100, 10), 10, 15, value=0.5)
    }

    # smoothing history arrays
    self.t_history = numpy.full(self.output.nbins(), 0.5)
    self.f_history = numpy.full(self.output.nbins(), 0.0)

  def get_samples(self):
    fmt = '<{}h'.format(self.stream.sample_size)
    byte_string = self.stream.read(self.stream.sample_size)
    return list(map(util.normalize, struct.unpack(fmt, byte_string)))

  def draw_time(self, samples, surface):
    width, height = surface.get_size()
    bin_width = width / self.output.nbins()

    s = self.sliders['smooth'].value
    for i in range(self.output.nbins()):
      power_i = samples[i]
      power_s = self.t_history[i]*s + power_i*(1-s)
      power = self.t_history[i] = power_s

      bin_height = power * height
      top = height - bin_height
      left = i * bin_width
      rect = (left, top, bin_width, 5) #bin_height)

      color = util.gradient(power, self.colorA, self.colorB)
      pygame.draw.rect(surface, color, rect)

  def draw_freq(self, samples, surface):
    width, height = surface.get_size()
    y_max = self.stream.sample_size // 2
    bin_width = width / self.output.nbins()

    yf = numpy.log(numpy.abs(numpy.fft.fft(samples))+1)/numpy.log(y_max)

    s = self.sliders['smooth'].value

    pull = 1 - self.sliders['pull'].value
    g = (self.output.nbins() - 1) * (self.stream.sample_size//2 - 1) * pull
    v, h = util.shift_inverse_consts(
        0, 1, self.output.nbins()-1, self.stream.sample_size//2-1, g
    )

    for x in range(self.output.nbins()):
      y = util.shift_inverse(x, g, v, h)

      power_i = yf[math.ceil(y)]
      power_s = self.f_history[x]*s + power_i*(1-s)
      power = self.f_history[x] = power_s
      if power > 1.0:
        power = 1.0

      bin_height = power * height
      top = height - bin_height
      left = x * bin_width
      rect = (left, top, bin_width, bin_height)
      color = util.gradient(power, self.colorA, self.colorB)
      pygame.draw.rect(surface, color, rect)

  def resize_history(self):
    self.t_history.resize(self.output.nbins())
    self.f_history.resize(self.output.nbins())

  def resize(self):
    width = self.output.width()
    height = self.height
    self.time_surface = pygame.Surface((width, height // 2))
    self.freq_surface = pygame.Surface((width, height // 2))
    self.surface = pygame.display.set_mode(
      (width, height), self.surface_flags
    )
    self.resize_history()

  def process_key(self, key):
    HEIGHT_DELTA = 100
    HEIGHT_MIN = 300

    SIZE_MIN = 1

    RATE_DELTA = 1000
    RATE_MIN = 0

    mods = pygame.key.get_mods()
    shift = mods & pygame.KMOD_SHIFT

    if key == ord('b'):
      if shift:
        self.output.inc_nbins()

        # push up sample size if necessary
        while self.output.nbins() > self.stream.sample_size:
          self.stream.sample_size *= 2

      else:
        self.output.dec_nbins()

      self.resize_history()

    if key == ord('h'):
      if shift:
        self.height += HEIGHT_DELTA
      elif self.height > HEIGHT_MIN:
        self.height -= HEIGHT_DELTA

      self.resize()

    if key == ord('n'):
      if shift:
        self.stream.sample_size *= 2
      elif self.stream.sample_size > SIZE_MIN:
        self.stream.sample_size /= 2

        # push down n_bins if necessary
        while self.output.nbins() > self.stream.sample_size:
          self.output.dec_nbins()

    if key == ord('r'):
      k = 1 if shift else -1
      if self.stream.sample_rate > RATE_MIN:
        self.stream.sample_rate += k * RATE_DELTA

    if key == ord('w'):
      if shift:
        self.output.inc_width()

        # push up sample size if necessary
        while self.output.nbins() > self.stream.sample_size:
          self.stream.sample_size *= 2

      else:
        self.output.dec_width()

      self.resize()

  def process_events(self):
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        self.exit()

      if event.type == pygame.KEYDOWN:
        self.process_key(event.key)

      if event.type == pygame.MOUSEMOTION:
        x = event.pos[0]

        R = self.stream.sample_rate
        N = self.stream.sample_size
        pull = 1 - self.sliders['pull'].value
        g = (self.output.nbins() - 1) * (N//2 - 1) * pull
        v, h = util.shift_inverse_consts(
            0, 1, self.output.nbins()-1, N//2-1, g
        )

        bin_width = self.output.width() / self.output.nbins()
        bin_index = math.floor(x / bin_width)
        self.mouse_frequency = util.shift_inverse(bin_index, g, v, h)*(R/2)/(N/2-1)

        for slider in self.sliders.values():
          if slider.moving:
            slider.set_value(x)

      if event.type == pygame.MOUSEBUTTONDOWN:
        for slider in self.sliders.values():
          if slider.get_handle_rect().collidepoint(event.pos):
            slider.moving = True

      if event.type == pygame.MOUSEBUTTONUP:
        for slider in self.sliders.values():
          slider.moving = False

  def exit(self):
    self.stream.close()
    pygame.display.quit()
    pygame.quit()
    sys.exit(0)

  def loop(self):
    self.process_events()

    surfaces = [self.time_surface, self.freq_surface, self.control_surface]
    for surface in surfaces:
      surface.fill(self.background_color)


    fields = {
      'sample rate': str(self.stream.sample_rate),
      'sample size': str(self.stream.sample_size),
      'pull ratio': '{0:.2f}'.format(self.sliders['pull'].value),
      'smoothing ratio': '{0:.2f}'.format(self.sliders['smooth'].value),
      'window width': str(self.output.width()),
      'window height': str(self.height),
      'bin count': str(self.output.nbins()),
      'bin width': str(self.output.width() // self.output.nbins()),
      'mouse frequency': str(int(self.mouse_frequency))
    }

    self.controls.draw(fields)

    for slider in self.sliders.values():
      slider.draw()

    samples = self.get_samples()
    self.draw_time(samples, self.time_surface)
    self.draw_freq(samples, self.freq_surface)
    self.stream._stream.read(self.stream._stream.get_read_available())

    self.surface.blit(self.time_surface, (0,0))
    self.surface.blit(self.freq_surface, (0,self.height // 2))
    self.surface.blit(self.control_surface, (0,0))

    pygame.display.flip()

if __name__ == '__main__':
  waves = Waves()
  while True:
    try:
      waves.loop()
    except KeyboardInterrupt:
      waves.exit()
